FROM ubuntu:18.04

# Install steamcmd
RUN dpkg --add-architecture i386   && \
    apt-get update -y              && \
    \
    echo steam steam/question select "I AGREE" | debconf-set-selections && \
    echo steam steam/license note '' | debconf-set-selections && \
    \
    DEBIAN_FRONTEND=noninteractive \
    apt-get install steamcmd       \
                    curl -y        && \
    rm -rf /var/lib/apt/lists/*    && \
    ln -s /usr/games/steamcmd /usr/local/bin/steamcmd && \
    useradd -m steam -u 5734

WORKDIR /home/steam

# Switch to steam user
USER steam

ENTRYPOINT ["steamcmd"]
CMD ["+login anonymous +quit"]
